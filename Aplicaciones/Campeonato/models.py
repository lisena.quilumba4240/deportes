from django.db import models

# Create your models here.


class Posicion(models.Model):
    id_pos = models.AutoField(primary_key=True)
    nombre_pos = models.CharField(max_length=150)
    descripcion_pos = models.TextField()

class Equipo(models.Model):
    id_equi = models.AutoField(primary_key=True)
    nombre_equi = models.CharField(max_length=500)
    siglas_equi = models.CharField(max_length=25)
    fundacion_equi = models.IntegerField()
    region_equi = models.CharField(max_length=25)
    numero_titulos_equi = models.IntegerField()

class Jugador(models.Model):
    id_jug = models.AutoField(primary_key=True)
    apellido_jug = models.CharField(max_length=500)
    nombre_jug = models.CharField(max_length=500)
    estatura_jug = models.FloatField()
    salario_jug = models.DecimalField(max_digits=10, decimal_places=2)
    estado_jug = models.CharField(max_length=25)
    fk_id_pos = models.ForeignKey('Posicion', on_delete=models.CASCADE)
    fk_id_equi = models.ForeignKey('Equipo', on_delete=models.CASCADE)
