from django.contrib import admin
from .models import Equipo
from .models import Jugador
from .models import Posicion
# Register your models here.
admin.site.register(Equipo)
admin.site.register(Jugador)
admin.site.register(Posicion)
