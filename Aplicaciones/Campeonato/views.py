from django.shortcuts import render, redirect
from django.core.files.storage import default_storage
from django.db.models.deletion import ProtectedError
from django.conf import settings
from django.contrib import messages
from .models import Posicion, Equipo, Jugador
from django.http import HttpResponseRedirect
# Create your views here.
# Create your views here.

#Posicion
def listadoPosiciones(request):
    posicionesBdd = Posicion.objects.all()
    return render(request, 'listadoPosiciones.html', {'posiciones':posicionesBdd})


def guardarPosicion(request):
    if request.method == 'POST':
        nombre_pos = request.POST.get("nombre_pos")
        descripcion_pos = request.POST.get("descripcion_pos")


        posicion = Posicion.objects.create(
            nombre_pos=nombre_pos,
            descripcion_pos=descripcion_pos

        )

        messages.success(request, 'POSICION GUARDADO EXITOSAMENTE')
        return redirect('/')

def eliminarPosicion(request, id_pos):
    try:
        posicion_eliminar = Posicion.objects.get(id_pos=id_pos)
        try:
            posicion_eliminar.delete()
            messages.success(request, 'POSICION ELIMINADO EXITOSAMENTE')
        except ProtectedError:
            messages.warning(request, 'No se puede eliminar la Posicion porque existen objetos relacionados.')
    except Posicion.DoesNotExist:
        messages.error(request, 'La Posicion no existe')

    return redirect('/')

def editarPosicion(request, id_pos):
    try:
        posicion_editar = Posicion.objects.get(id_pos=id_pos)
        posiciones_bdd = Posicion.objects.all()
        return render(request, 'editarPosicion.html', {'posicion': posicion_editar, 'posiciones': posiciones_bdd})
    except Posicion.DoesNotExist:
        messages.error(request, 'La posicion no existe')
        return redirect('/')


#procesarActualizarPosicion
def procesarActualizacionPosicion(request):
        if request.method == 'POST':
            id_pos = request.POST.get("id_pos")
            nombre_pos = request.POST.get("nombre_pos")
            descripcion_pos = request.POST.get("descripcion_pos")


        try:
            posicionEditar = Posicion.objects.get(id_pos=id_pos)

            posicionEditar.nombre_pos = nombre_pos
            posicionEditar.descripcion_pos = descripcion_pos
            posicionEditar.save()

            messages.success(request, 'POSICION ACTUALIZADO EXITOSAMENTE')
        except Posicion.DoesNotExist:
            messages.error(request, 'El equipo no existe')

        return redirect('/')

#Equipo
def listadoEquipo(request):
    equipoBdd = Equipo.objects.all()
    return render(request, 'listadoEquipos.html', {'equipos':equipoBdd})

def guardarEquipo(request):
    if request.method == 'POST':
        nombre_equi = request.POST.get("nombre_equi")
        siglas_equi = request.POST.get("siglas_equi")
        fundacion_equi = request.POST.get("fundacion_equi")
        region_equi = request.POST.get("region_equi")
        numero_titulos_equi = request.POST.get("numero_titulos_equi")


        equipo = Equipo.objects.create(
            nombre_equi=nombre_equi,
            siglas_equi=siglas_equi,
            fundacion_equi=fundacion_equi,
            region_equi=region_equi,
            numero_titulos_equi=numero_titulos_equi

        )

        messages.success(request, 'POSICION GUARDADO EXITOSAMENTE')
        return redirect('/listadoEquipo')

def eliminarEquipo(request, id_equi):
    try:
        equipo_eliminar = Equipo.objects.get(id_equi=id_equi)
        try:
            equipo_eliminar.delete()
            messages.success(request, 'EQUIPO ELIMINADO EXITOSAMENTE')
        except ProtectedError:
            messages.warning(request, 'No se puede eliminar la Posicion porque existen objetos relacionados.')
    except Equipo.DoesNotExist:
        messages.error(request, 'La Posicion no existe')

    return redirect('/listadoEquipos')

def editarEquipo(request, id_equi):
    try:
        equipo_editar = Equipo.objects.get(id_equi=id_equi)
        equipos_bdd = Equipo.objects.all()
        return render(request, 'editarEquipo.html', {'equipo': equipo_editar, 'equipos': equipos_bdd})
    except Equipo.DoesNotExist:
        messages.error(request, 'El equipo no existe')
        return redirect('/listadoEquipo')

# Procesar la actualización del equipo
def procesarActualizacionEquipo(request):
    if request.method == 'POST':
        id_equi = request.POST.get("id_equi")
        nombre_equi = request.POST.get("nombre_equi")
        siglas_equi = request.POST.get("siglas_equi")
        fundacion_equi = request.POST.get("fundacion_equi")
        region_equi = request.POST.get("region_equi")
        numero_titulos_equi = request.POST.get("numero_titulos_equi")

    try:
        equipo_editar = Equipo.objects.get(id_equi=id_equi)

        equipo_editar.nombre_equi = nombre_equi
        equipo_editar.siglas_equi = siglas_equi
        equipo_editar.fundacion_equi = fundacion_equi
        equipo_editar.region_equi = region_equi
        equipo_editar.numero_titulos_equi = numero_titulos_equi
        equipo_editar.save()

        messages.success(request, 'EQUIPO ACTUALIZADO EXITOSAMENTE')
    except Equipo.DoesNotExist:
        messages.error(request, 'El equipo no existe')

    return redirect('/listadoEquipo')

#JUGADORES
def listadoJugadores(request):
    jugadoresBdd = Jugador.objects.all()
    posiciones = Posicion.objects.all()
    equipos = Equipo.objects.all()
    return render(request, 'listadoJugadores.html', {'jugadores':jugadoresBdd, 'posiciones': posiciones, 'equipos': equipos})
def guardarJugador(request):
    if request.method == 'POST':
        apellido_jug = request.POST.get("apellido_jug")
        nombre_jug = request.POST.get("nombre_jug")
        estatura_jug = request.POST.get("estatura_jug")
        salario_jug = request.POST.get("salario_jug")
        estado_jug = request.POST.get("estado_jug")
        fk_id_pos = request.POST.get("fk_id_pos")
        fk_id_equi = request.POST.get("fk_id_equi")

        try:
            nueva_posicion = Posicion.objects.get(id_pos=fk_id_pos)
            nuevo_equipo = Equipo.objects.get(id_equi=fk_id_equi)
            nuevo_jugador = Jugador(
                apellido_jug=apellido_jug,
                nombre_jug=nombre_jug,
                estatura_jug=estatura_jug,
                salario_jug=salario_jug,
                estado_jug=estado_jug,
                fk_id_pos=nueva_posicion,
                fk_id_equi=nuevo_equipo
            )
            nuevo_jugador.save()
            messages.success(request, 'Jugador creado exitosamente')
        except (Posicion.DoesNotExist, Equipo.DoesNotExist):
            messages.error(request, 'Posición o equipo no encontrado')

        return redirect('/listadoJugadores')

    else:
        posiciones = Posicion.objects.all()
        equipos = Equipo.objects.all()
        return render(request, 'listadoJugadores.html', {'posiciones': posiciones, 'equipos': equipos})
def eliminarJugador(request, id_jug):
    try:
        jugador_eliminar = Jugador.objects.get(id_jug=id_jug)
        try:
            jugador_eliminar.delete()
            messages.success(request, 'EQUIPO ELIMINADO EXITOSAMENTE')
        except ProtectedError:
            messages.warning(request, 'No se puede eliminar la Posicion porque existen objetos relacionados.')
    except Jugador.DoesNotExist:
        messages.error(request, 'La Posicion no existe')

    return redirect('/listadoJugadores')

def editarJugador(request, id_jug):
    try:
        jugador_editar = Jugador.objects.get(id_jug=id_jug)
        jugador_bdd = Jugador.objects.all()
        posiciones = Posicion.objects.all()
        equipos = Equipo.objects.all()
        return render(request, 'editarJugador.html', {'jugador': jugador_editar, 'jugadores': jugador_bdd, 'posiciones': posiciones, 'equipos': equipos})
    except Jugador.DoesNotExist:
        messages.error(request, 'El equipo no existe')
        return redirect('/listadoJugadores')

def procesarActualizacionJugador(request):
    if request.method == 'POST':
        jugador_id = request.POST.get('id_jug')
        jugador = Jugador.objects.get(id_jug=jugador_id)

        jugador.apellido_jug = request.POST.get('apellido_jug')
        jugador.nombre_jug = request.POST.get('nombre_jug')
        jugador.estatura_jug = request.POST.get('estatura_jug')
        jugador.salario_jug = request.POST.get('salario_jug')
        jugador.estado_jug = request.POST.get('estado_jug')
        jugador.fk_id_pos_id = request.POST.get('fk_id_pos')
        jugador.fk_id_equi_id = request.POST.get('fk_id_equi')

        jugador.save()

        return redirect('/listadoJugadores')  # Redirige a la página principal o a donde desees después de la actualización

    return redirect('/listadoJugadores')
