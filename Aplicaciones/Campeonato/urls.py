from django.urls import path
from . import views


urlpatterns = [
    path('', views.listadoPosiciones),
    path('guardarPosicion/', views.guardarPosicion),
    path('eliminarPosicion/<int:id_pos>/', views.eliminarPosicion),
    path('editarPosicion/<int:id_pos>/', views.editarPosicion),
    path('procesarActualizacionPosicion/', views.procesarActualizacionPosicion),
    #equipo
    path('listadoEquipo/', views.listadoEquipo),
    path('guardarEquipo/', views.guardarEquipo),
    path('eliminarEquipo/<int:id_equi>/', views.eliminarEquipo),
    path('editarEquipo/<int:id_equi>/', views.editarEquipo),
    path('procesarActualizacionEquipo/', views.procesarActualizacionEquipo),
    #jugadores

    path('listadoJugadores/', views.listadoJugadores),
    path('guardarJugador/', views.guardarJugador),
    path('eliminarJugador/<int:id_jug>/', views.eliminarJugador),
    path('editarJugador/<int:id_jug>/', views.editarJugador),
    path('procesarActualizacionJugador/', views.procesarActualizacionJugador),
]
